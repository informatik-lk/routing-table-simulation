# Routing table simulation
This simulates the communication and data exchange in a router network. This has to be run on multiple hosts in the same network

## New implementation using UDP
UDP bears the increased risk of packet loss, but I don't really are about that. I was not onboard with the decision to use UDP instead of TCP in the first place, so I don't feel obliged to implement useless features to ensure that everything arrives correctly when another, superior, protocol already has this built in.

## Setup
### Environment variables
```dotenv
HOSTNAME=0.0.0.0
PORT=3333
ID=JohnDoe
```
### Configuring your neighbors
Create a `src/routers.json` file that contains your neighbors in the folliowing format:

```json
[
  [
    "NEIGHBOR_ID",
    {
      "ip": "NEIGHBOR_IP",
      "gateway": "NEIGHBOR_ID", 
      "cost": 0
    }
  ],
  ...
]
```

### Dependencies
- [Bun](https://bun.sh)

### Running the program
```bash
bun run src/index.ts
```
Bun dependencies are installed automatically when you run the program for the first time.