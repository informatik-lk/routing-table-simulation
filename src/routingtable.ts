export default class RoutingTable {
    private table: Map<string, { ip: string, gateway: string, distance: number }>;
    private neighbors: string[] = [];

    constructor(neigbhors: Map<string, { ip: string, gateway: string, distance: number }>) {
        this.table = neigbhors;
        neigbhors.forEach((_value, key) => this.neighbors.push(key));
    }

    public getNeighbours() {
        return this.neighbors;
    }

    public getEntry(name: string) {
        return this.table.get(name);
    }

    public importData(source: string, data: {id: string, ip: string, distance: number}[]) {
        if (!this.table.has(source)) {
            console.log(this.table);
            console.log(source);
            throw new Error('Gateway unknown');
        }

        let gatewayDistance = this.table.get(source)!.distance;

        let changed = false;

        for (let dataset of data) {
            if (dataset.id === process.env.ID) {
                continue;
            }

            if (!this.table.has(dataset.id)) {
                this.table.set(dataset.id, { ip: dataset.ip, gateway: source, distance: dataset.distance + gatewayDistance });

                console.log(`➕ Added ${dataset.id} to routing table`);
                changed = true;
            } else if (dataset.distance + gatewayDistance < this.table.get(dataset.id)!.distance) {
                this.table.set(dataset.id, { ip: dataset.ip, gateway: source, distance: dataset.distance + gatewayDistance });

                console.log(`✍️Updated ${dataset.id} in routing table`);
                changed = true;
            } else {
                console.log(`❎ No changes to ${dataset.id}`);
            }
        }

        return changed;
    }

    public exportData() {
        return Array.from(this.table).map(entry => {
            return {
                id: entry[0],
                ip: entry[1].ip,
                distance: entry[1].distance
            };
        });
    }

    public getTable() {
        return this.table;
    }
}