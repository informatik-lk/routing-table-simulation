import { createSocket } from 'dgram';
import RoutingTable from "./routingtable.ts";
import fs from 'fs';

const routingTable = new RoutingTable(new Map(JSON.parse(fs.readFileSync('src/routes.json', 'utf-8'))));

let server = createSocket('udp4');

server.on('message', (msg: Buffer, rinfo) => {
    if (!routingTable.getNeighbours().map(n => routingTable.getEntry(n)!.ip).includes(rinfo.address)) {
        console.log('🔒 Unauthorized connection attempt');
        return;
    }

    console.log(`⛓️ Connection from ${rinfo.address}`);

    try {
        let jsonData: {
            sender_id: string,
            routes: { id: string, ip: string, distance: number }[]
        } = JSON.parse(msg.toString());

        console.log(`📩 Data received from ${jsonData.sender_id} at ${rinfo.address}: ${JSON.stringify(jsonData.routes)}`);

        let changed = routingTable.importData(jsonData.sender_id, jsonData.routes);

        if (changed) {
            console.log('☑️Routing table updated');
            broadcastData();
        } else {
            console.log('❎ No changes in routing table');
            console.log(routingTable.getTable());
        }
    } catch (e) {
        console.log('❌ Invalid JSON received');
        console.log(msg.toString());
        console.log(e);
    }
});

function broadcastData() {
    routingTable.getNeighbours().forEach(n => {
        let neighbour = routingTable.getEntry(n)!;
        try {
            let message = Buffer.from(JSON.stringify({ sender_id: process.env.ID, routes: routingTable.exportData() }));
            server.send(message, parseInt(process.env.PORT as string), neighbour.ip, (err) => {
                if (err) {
                    console.log(`Failed to send data to ${n} at ${neighbour.ip}`);
                } else {
                    console.log(`Data sent to ${n} at ${neighbour.ip}`);
                }
            });
        } catch (e) {
            console.log(`Failed to send data to ${n} at ${neighbour.ip}`);
        }
    });
}

server.bind(parseInt(process.env.PORT as string), process.env.HOSTNAME as string, () => {
    console.log(`🚀 Server started and listening on ${process.env.HOSTNAME}`);
    broadcastData();
});
